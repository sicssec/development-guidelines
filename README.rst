
SICS Security Lab software development guidelines
=================================================


This document strives to explain our minimal development guidelines in a simple and practical manner.
Note that this is not a document about
`secure software development <https://www.cert.org/secure-coding/publications/index.cfm>`_,
but mainly a guideline document for our master thesis students who happen to
write software.


Do
--

- Use a source code management and versioning tool.

  - We recommend `git <https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository>`_

  - We recommend you host your projects on `bitbucket <https://bitbucket.org/>`_ or `gitlab <https://gitlab.com>`_ in a private repo

- Ensure different stages of project are identifiable in the source code

  - `Commit changes often <https://blog.codinghorror.com/check-in-early-check-in-often/>`_

  - Mark special milestones (e.g. releases, experiments) using `tags <https://git-scm.com/book/en/v2/Git-Basics-Tagging>`_ or similar

- Include the minimal files in your project

  - README -- (we recommend README.rst)

  - LICENSE -- project license file

  - .gitignore -- used to exclude unnecessary files

  - Makefile -- (or similar) for automatic builds



Do not
------

- Do not include software created by other people in your code without including
  reference and the original license

  - Verify that their license is not “infecting” your code

- Do not break the build

  - Do not commit changes that fail to build

  - Do not have a build process that requires manual work

  - Do not reference or use files outside the repo (e.g. files on your personal laptop)

- Do not break git

  - Once published, do not alter the code history (there may be some exceptions)

  - Do not include large files, auto-generated files, backup and junk files

  - Do not artificially inflate the commit (e.g. by changing the indentation)


You may also...
---------------

- Publish the code

  - Perform code review before publishing the code

  - Use the security lab `team page <https://bitbucket.org/sicssec>`_

- Make it more accessible

  - Use `reStructuredText <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`_ for text files (e.g. README.rst instead of README)

  - Use the same coding and indentation style everywhere

    - we recommend `Linux Kernel Coding Style <https://www.kernel.org/doc/Documentation/process/coding-style.rst>`_ for C/C++ projects

    - we recommend `Oracle Code Conventions <http://www.oracle.com/technetwork/java/javase/documentation/codeconvtoc-136057.html>`_ for Java projects

  - Provide guidelines on how to set up the machine (e.g. apt-get commands for ubuntu/debian)

    - Alternatively, provide a build machine (e.g. Vagrant or Docker scripts)

  - Keep the documentation in the docs/ folder

  - Use at most two different programming languages in the same project

  - If applicable, provide deployment scripts

- Make it more robust

  - Provide automatic unit tests

  - Use the issue system for registering and managing bugs
